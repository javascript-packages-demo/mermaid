# [mermaid](https://www.npmjs.com/package/mermaid)

Generation of diagram and flowchart from text in a similar manner as markdown https://mermaidjs.github.io/

https://stackedit.io/

```mermaid
flowchart LR
  A --> B
```